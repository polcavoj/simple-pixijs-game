import * as ECSA from '../libs/pixi-component';
import {GameModel} from './game-model';
import GameFactory from './game-factory';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {Messages} from './constants';
import {checkTime} from './utils/functions';

export default class MenuControllerComponent extends BananaRaveBaseComponent {
  model: GameModel;
  factory: GameFactory;
  cmp: ECSA.KeyInputComponent;
  gameStarted: boolean;
  gameIsRunning: boolean;
  gameOver: boolean;
  protected lastUpdate = 0;

  constructor() {
    super();
    this.gameStarted = false;
    this.gameIsRunning = false;
    this.gameOver = false;
  }

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_OVER);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_OVER) {
      this.gameStarted = false;
      this.gameOver = true;
      this.gameIsRunning = false;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.cmp === undefined) {
      this.cmp = this.scene.stage.findComponentByName<ECSA.KeyInputComponent>(ECSA.KeyInputComponent.name);
    }

    if (!this.gameOver && !this.gameIsRunning && !this.gameStarted && (this.cmp.isKeyPressed(ECSA.Keys.KEY_LEFT) || this.cmp.isKeyPressed(ECSA.Keys.KEY_RIGHT) || this.cmp.isKeyPressed(ECSA.Keys.KEY_SPACE))) {
      if(checkTime(this.lastUpdate, absolute, 5)) {
        this.lastUpdate = absolute;
        this.sendMessage(Messages.GAME_START);
        this.gameIsRunning = true;
        this.gameStarted = true;
        this.owner.pixiObj.visible = false;
      }
    } else if (!this.gameOver && !this.gameIsRunning && this.gameStarted && (this.cmp.isKeyPressed(ECSA.Keys.KEY_SPACE))) {
      if(checkTime(this.lastUpdate, absolute, 5)) {
        this.lastUpdate = absolute;
        this.sendMessage(Messages.GAME_RUNNING);
        this.gameIsRunning = true;
        this.owner.pixiObj.visible = false;
      }
    } else if (!this.gameOver && !this.gameIsRunning && this.cmp.isKeyPressed(ECSA.Keys.KEY_R)) {
      if(checkTime(this.lastUpdate, absolute, 5)) {
        this.lastUpdate = absolute;
        this.sendMessage(Messages.GAME_RESTART);
        this.gameIsRunning = true;
        this.gameStarted = true;
        this.model.reset();
        this.scene.invokeWithDelay(0.5, () => this.factory.resetGame(this.scene));
      }
    } else if (!this.gameOver && this.gameIsRunning && (this.cmp.isKeyPressed(ECSA.Keys.KEY_SPACE))) {
      if(checkTime(this.lastUpdate, absolute, 5)) {
        this.lastUpdate = absolute;
        this.sendMessage(Messages.GAME_PAUSED);
        this.gameIsRunning = false;
        this.owner.pixiObj.visible = true;
      }
    }
    if (this.gameOver && !this.gameStarted && !this.gameIsRunning && this.cmp.isKeyPressed(ECSA.Keys.KEY_SPACE)) {
      if(checkTime(this.lastUpdate, absolute, 5)) {
        this.lastUpdate = absolute;
        this.gameOver = false;
        this.model.reset();
        this.scene.invokeWithDelay(1, () => this.factory.resetGame(this.scene));
      }
    }
  }
}
